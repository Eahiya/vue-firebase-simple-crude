import Vue from 'vue'

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import UserPages from '../pages/Users'
import AddUser from '../pages/AddUser.vue'
import EditUser from '../pages/EditUser.vue'
const routes = [
    {
        path: '/',
        component: UserPages,
        name: 'home'
    },
    {
        path: '/add-user',
        name: 'add-user',
        component:AddUser
    },
    {
        path: '/edit-user/:slug',
        name: 'edit-user',
        component: EditUser,

    }
]

const router = new VueRouter({
    routes,
    mode: 'history'
});


export default router;

